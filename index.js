// console.log("Hello World");

function checkAddition(num1, num2) {

	console.log	("Displayed sum of " + num1 + " and " + num2) ;	 
	console.log (num1 + num2) ;

}

checkAddition(20, 10);


function checkSubtraction(num1, num2) {

	console.log	("Displayed difference of " + num1 + " and " + num2) ;	 
	console.log (num1 - num2) ;
}

checkSubtraction(20, 10);


function returnMultiplication(num1, num2) {

	console.log("The product of " + num1 + " and " +num2);
	return num1 * num2;
	
}

let product = returnMultiplication(20, 10);
console.log(product);


function returnDivision(num1, num2) {

	console.log("The quotient of " + num1 + " and " +num2);
	return num1 / num2;
}

let quotient = returnDivision(20, 10);
console.log(quotient);


function circleArea (radius) {
	console.log ("The result of getting the area of a circle with " + radius +  " radius:");
	let pie = 3.14159;
	return pie * radius **2;
}
let circleAreaRadius = circleArea (15);
console.log (circleAreaRadius);


function getAverage (num1, num2, num3, num4) {
	console.log ("The average of " + num1 + ", " + num2 + ", " + num3 + ", " + num4 + ":") ;
	return 	(num1 + num2 + num3 + num4) / 4;
}
let averageVar = getAverage (20, 40, 60, 80);
console.log	(averageVar);


function getScore (num1, num2) {
	console.log ("Is " + num1 + "/" + num2 + " a passing score?") ;
	let percent = (num1/num2) * 100;
	let isPassed = percent >= 75;
	return isPassed;
}
let isPassingScore = getScore (38, 50);
console.log (isPassingScore);
